from gc import get_stats
from math import floor
import discord
import requests
import json 
import datetime
from discord.ext import commands, tasks
import time
from sqlalchemy import create_engine, select
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, String, Integer, BigInteger, Date, Boolean, Numeric, ForeignKey
from sqlalchemy.orm import relationship
from queue import PriorityQueue
import time


engine = create_engine('mysql+pymysql://root:passw@localhost:3306/stats_bot')
Session = sessionmaker(bind=engine)

B = 1000*1000*1000
TOKEN = ''
GUILD = ''
GUILD_TEST = ''
channel_test = 
channel = 

Base = declarative_base()

class Collection(Base):
    __tablename__ = 'collection'
    id=Column(Integer, primary_key=True)
    symbol=Column('symbol', String(200))
    name=Column('name', String(200))
    description=Column('description', String(500))
    image=Column('image', String(300))
    stats=relationship("Stats", cascade="all, delete")
    

    def __init__(self, symbol, name, description, image):
        self.symbol = symbol
        self.name = name
        self.description = description[:490]
        self.image = image
        self.delta_volume = 0

class Stats(Base):
    __tablename__ = 'stats'
    id=Column(Integer, primary_key=True)
    floorPrice = Column(BigInteger)
    listedCount = Column(BigInteger)
    volumeAll = Column(BigInteger)
    symbol_id = Column(Integer, ForeignKey('collection.id'))

    def __init__(self,floorPrice, listedCount,volumeAll):
        self.floorPrice = floorPrice
        self.listedCount = listedCount
        self.volumeAll = volumeAll


Base.metadata.create_all(engine)
session = Session()
client = discord.Client()

def api_get_stats(symbol)->list:
    list_collection = []
    report_url = 'http://api-mainnet.magiceden.dev/v2/collections/{0}/stats'.format(symbol)
    try:
        api_response = requests.get(report_url,  timeout=10)
        if api_response.status_code != 200:
            return False
        api_response = json.loads(api_response.text)

        try:
            floorPrice = api_response['floorPrice']
        except KeyError as e:
            floorPrice = 0

        try:
            listedCount=api_response['listedCount']
        except KeyError as e:
            listedCount = 0

        try:
            volumeAll=api_response['volumeAll']
        except KeyError as e:
            volumeAll = 0

        stats = Stats(floorPrice = floorPrice, listedCount = listedCount, 
                volumeAll = volumeAll)
        if stats:
            return stats
        else:
            return "error"
    except Exception as ex:
        print(ex)

def api_get_collections(offset, limit)->list:
    list_collection = []
    params = {"offset" : offset, "limit":limit}
    report_url = f'http://api-mainnet.magiceden.dev/v2/collections' 
    api_response = requests.get(report_url, params=params)
    print("offset"+str(offset))
    if api_response.status_code != 200:
        api_response = requests.get(report_url, params=params)
        if api_response.status_code != 200:
            return False
    api_response = json.loads(api_response.text)
    for col in api_response:
        start_time = time.time()
        print(col['symbol'])
        stat = api_get_stats(col['symbol'])
        print("api_get_stats %s seconds" % ( time.time() - start_time ) )

        if stat and stat.volumeAll > 500*B:
            c = Collection(symbol = col['symbol'], name = col['name'], description = col['description'],
            image = col['image'])
            list_collection.append(c)
        
    return list_collection


def get_or_create(session, model, filter, collection):
    instance = session.query(model).filter_by(symbol=filter).first()
    if instance:
        return instance
    else:
        session.add(collection)
        session.commit()
        return collection

def get_collections():
    num_elem = 200
    offset = 0 
    start_time = time.time()
    list_collection = []
    l = api_get_collections(offset,num_elem)
    list_collection = list_collection + l 
    while(len(l) != 0):
        l = api_get_collections(offset,num_elem)
        if l == False:
            offset = offset + num_elem
            l = ['error']
            continue
        list_collection = list_collection + l 
        offset = offset + num_elem
    print("api_get_collections --- %s seconds ---" % (time.time() - start_time))
    #
    for collection in list_collection:
        try:
            a = get_or_create(session, Collection, collection.symbol, collection )
        except Exception as ex:
            pass
    session.commit()
    session.close()
    
def get_stats(symbol):
    ret = api_get_stats(symbol)
    if type(ret)==bool:
        return False
    else:
        return ret

@tasks.loop(seconds=60*10)
async def check_volume():
    prev_volume = 0
    act_volume = 0
    max_volume = 0
    collection = None
    q = PriorityQueue(maxsize = 5)
    list_collections =  select(Collection)
    result = session.execute(list_collections)
    start_time = time.time()
    for col in result.scalars():
        try:
            result = None
            prev_volume = 0
            act_volume = 0
            volume_symbol = select(Stats.volumeAll).where(Stats.symbol_id == col.id).order_by(Stats.id.desc())
            result = session.execute(volume_symbol).first()
            if result:
                prev_volume = result[0]
            stat = get_stats(col.symbol)
            if stat == False:
                continue
            stat.symbol_id = col.id
            session.add(stat)
            session.commit()
            result = session.execute(volume_symbol).first()
            act_volume = result[0]
            col.delta_volume = (act_volume - prev_volume)/B
            element = None
            if not q.full():
                q.put((col.delta_volume, id(col), col))
            else:
                if col.delta_volume > q.queue[0][0]:
                    q.get()
                    q.put((col.delta_volume, id(col), col))

        except Exception as ex:
            print(ex,col.name)
    print("--- %s seconds ---" % (time.time() - start_time))
    text = "The top Project by volume since 30 minutes are: 📈 \n\n"
    list_coll = []
    for queue_coll in q.queue:
        list_coll.append(queue_coll[2])
    #sort collection  
    #list_coll.sort(key=lambda x: x.delta_volume, reverse=True)
    for coll in list_coll:
        text = text + "**" +str(coll.name) + "** " +"\n"
        text = text + "Volume: " +  "{:.2f}".format(coll.delta_volume) + "\n"
        text = text +"--------------------------------\n"
    channel = client.get_channel(channel)
    await channel.send(text)
    

@tasks.loop(seconds=60*60*24)
async def check_collection():
    start_time = time.time()
    l = get_collections()
    print("time to get collection --- %s seconds ---" % (time.time() - start_time))


@client.event
async def on_ready():
    for guild in client.guilds:
        if guild.name == GUILD:
            break
    print(
        f'{client.user} is connected to the following guild:\n'
        f'{guild.name}(id: {guild.id})'
    )
    await client.wait_until_ready()

    if not check_collection.is_running():
        check_collection.start()

    if not check_volume.is_running():
            check_volume.start() 


channel = client.get_channel(GUILD)
client.run(TOKEN)


